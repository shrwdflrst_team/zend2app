<?php

namespace Contact\Model;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class Contact implements EventManagerAwareInterface
{
    private $_db;
    protected $events;
    
    public function __construct(\PDO $db)
    {
        $this->_db = $db;
    }
    
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        
        $this->events = $events;
        
        return $this;
    }
    
    public function getEventManager()
    {
        if(null === $this->events) {
            $this->setEventManager(new EventManager());
        }
        
        return $this->events;
    }
    
    public function getAllRows()
    {
        $sql = "select * from contact";
        $stat = $this->_db->query($sql);
        
        return $stat->fetchAll();
    }
    
    public function addRow($data)
    {
        $this->getEventManager()->trigger('event.insert', $this);
        
        $sql = sprintf("INSERT INTO `contact` (name, email, phone) VALUES ('%s', '%s', '%s')",
            $data['name'],
            $data['email'],
            $data['phone']
        );
        
        return $this->_db->exec($sql);
    }
    
    public function getRow($id)
    {
        $sql = sprintf("SELECT * FROM `contact` WHERE id = ?");
        $stat = $this->_db->prepare($sql);
        $stat->execute(array($id));
        
        return $stat->fetch();
    }
    
    public function updateRow($data, $id)
    {
        $this->getEventManager()->trigger('event.edit', $this);
        
        $sql = sprintf("UPDATE `contact` SET name='%s', email='%s', phone='%s' WHERE id=%d",
            $data['name'],
            $data['email'],
            $data['phone'],
            $id
        );
        
        return $this->_db->exec($sql);
    }
    
    public function delRow($id)
    {
        $this->getEventManager()->trigger('event.delete', $this);
        
        $sql = sprintf("DELETE FROM contact WHERE id=%d", $id);
        
        return $this->_db->exec($sql);
    }    
}
